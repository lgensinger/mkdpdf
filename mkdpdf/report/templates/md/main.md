# Current Week (DATE_PUBLISH)

REPORT_NARRATIVE

## Schedule

### Tasks

REPORT_TASKS

## Potential Blockers

### Risks

REPORT_RISKS

## Key Events

REPORT_KEY_EVENTS

## Project Context

### About

#### Project Requisite

REPORT_PROJECT_REQUISITE

REPORT_PROJECT_ABOUT
