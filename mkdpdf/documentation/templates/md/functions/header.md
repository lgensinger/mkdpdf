# Functions

[_PACKAGE_NAME vPACKAGE_VERSION_](URL_RELEASE)

Functions are part of the package,  **PACKAGE_NAME:** _PACKAGE_DESCRIPTION_, and not associated with a package class.

To get started with installation and setup, make sure to follow the guidance of the [README](URL_GIT). When the default for a given parameter is listed as `configuration.XX` or `os.environ['XX']` this indicates that the value is populated from the environment via the package `configuration` or directly from the environmental variable.
