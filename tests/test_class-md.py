import os

from mkdpdf import configuration
from mkdpdf.md.md import MD

FILE_PATH = os.path.join(configuration.DIRECTORY_PATH_PACKAGE, "test")

def test():

    # initialize
    m = MD(FILE_PATH)

    ##################
    # CONSTRUCT TEST #
    ##################
    result = m.construct("header", "main", "footer")

    assert isinstance(result, str)

    ###############
    # RENDER TEST #
    ###############
    m.render(result, FILE_PATH)

    assert os.path.exists(FILE_PATH)

    with open(FILE_PATH, "r") as f:
        assert f.read() == result
